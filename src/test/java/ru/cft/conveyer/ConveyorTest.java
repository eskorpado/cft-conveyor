package ru.cft.conveyer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ConveyorTest {

    private static final List<Integer> QUEUE_A = asList(1, 2, 3, 4, 5);
    private static final List<Integer> QUEUE_B = asList(6, 7, 8, 9, 10, 11, 12);
    private static final List<Integer> CROSS_POINTS = asList(1, 3);

    private static Stream<Conveyor> provideConveyor() {
        return Stream.of(
                new LinkedListConveyor(QUEUE_A, QUEUE_B, CROSS_POINTS),
                new SplitedDequeueConveyor(QUEUE_A, QUEUE_B, CROSS_POINTS)
        );
    }

    @ParameterizedTest
    @MethodSource("provideConveyor")
    void pushToQueue(Conveyor conveyor) {
        assertEquals(conveyor.pushToQueueA(13), 5);
        assertEquals(conveyor.getQueueA(), asList(13, 1, 2, 3, 4));
        assertEquals(conveyor.getQueueB(), asList(6, 1, 8, 3, 10, 11, 12));

        assertEquals(conveyor.pushToQueueB(14), 12);
        assertEquals(conveyor.getQueueB(), asList(14, 6, 1, 8, 3, 10, 11));
        assertEquals(conveyor.getQueueA(), asList(13, 6, 2, 8, 4));
    }

    @Test
    void shouldCheckThatQueuesNotEmpty() {
        assertThrows(
                IllegalArgumentException.class,
                () -> new LinkedListConveyor(Collections.emptyList(), QUEUE_B, CROSS_POINTS),
                "Empty initial queues are not allowed"
        );
    }

    @Test
    void shouldCheckThatQueuesAreNotCrossAtTail() {
        int queueAMaxIndex = QUEUE_A.size() - 1;
        assertThrows(
                IllegalArgumentException.class,
                () -> new LinkedListConveyor(QUEUE_A, QUEUE_B, asList(1, queueAMaxIndex)),
                "Queues should'n cross at tail"
        );
    }
}