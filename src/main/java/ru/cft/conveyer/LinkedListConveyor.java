package ru.cft.conveyer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

/**
 * Простая реализация конвеера через списки
 * В качестве внутреннего представления очереди использован класс LinkedList,
 * т.к. реализует интерфейс Deque (методы addFirst, pollLast)
 */
@Component
@Profile("linkedList")
public class LinkedListConveyor extends AbstractConveyor<LinkedList<Integer>> {

    LinkedListConveyor(
            @Value("#{'${queuea}'.split(',')}") List<Integer> queueA,
            @Value("#{'${queueb}'.split(',')}") List<Integer> queueB,
            @Value("#{'${crosspoints}'.split(',')}") List<Integer> crossPoints) {
        super(queueA, queueB, crossPoints);
    }

    @Override
    public int pushToQueueA(int value) {
        return pushToQueue(queueA, queueB, value);
    }

    @Override
    public int pushToQueueB(int value) {
        return pushToQueue(queueB, queueA, value);
    }

    @Override
    protected LinkedList<Integer> mapInnerQueue(List<Integer> queue) {
        return new LinkedList<>(queue);
    }

    @SuppressWarnings("ConstantConditions")
    protected synchronized int pushToQueue(LinkedList<Integer> primary, LinkedList<Integer> secondary, int value) {
        primary.addFirst(value);
        crossPoints.forEach(point -> secondary.set(point, primary.get(point)));
        return primary.pollLast();
    }

    @Override
    protected List<Integer> getQueue(LinkedList<Integer> queue) {
        return queue;
    }
}
