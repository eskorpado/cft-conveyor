package ru.cft.conveyer;

import java.util.List;

abstract class AbstractConveyor<InnerQueue> implements Conveyor {
    final InnerQueue queueA;
    final InnerQueue queueB;
    final List<Integer> crossPoints;

    protected AbstractConveyor(
            List<Integer> queueA,
            List<Integer> queueB,
            List<Integer> crossPoints) {
        int max = crossPoints
                .stream()
                .mapToInt(Integer::intValue)
                .max()
                .orElse(0);
        if (queueA.size() == 0 || queueB.size() == 0) {
            throw new IllegalArgumentException("Empty initial queues are not allowed");
        }
        if (queueA.size() <= max + 1 || queueB.size() <= max + 1) {
            throw new IllegalArgumentException("Queues should'n cross at tail");
        }
        this.crossPoints = crossPoints;
        this.queueA = mapInnerQueue(queueA);
        this.queueB = mapInnerQueue(queueB);
    }

    @Override
    public int pushToQueueA(int value) {
        return pushToQueue(queueA, queueB, value);
    }

    @Override
    public int pushToQueueB(int value) {
        return pushToQueue(queueB, queueA, value);
    }

    @Override
    public List<Integer> getQueueA() {
        return getQueue(queueA);
    }

    @Override
    public List<Integer> getQueueB() {
        return getQueue(queueB);
    }

    /**
     * @param queue начальное состояние очереди из конфигурации
     * @return очередь, представленная в удобном для дальнейшей работы виде
     */
    protected abstract InnerQueue mapInnerQueue(List<Integer> queue);

    /**
     * Добавить элемент в очередь
     *
     * @param primary   очередь, в которую добавляем элемент
     * @param secondary связная очередь
     * @param value     добавляемый элемент
     * @return последний элемент (удаляется) из primary очереди
     */
    protected abstract int pushToQueue(InnerQueue primary, InnerQueue secondary, int value);

    /**
     * Получить состояние очереди
     */
    protected abstract List<Integer> getQueue(InnerQueue queue);
}
