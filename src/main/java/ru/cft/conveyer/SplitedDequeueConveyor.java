package ru.cft.conveyer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Конвеер реализованный посредством разделения очереди на отрезки:
 * Должен работать заметно быстрее на длинных очередях т.к. для каждого отрезка между точками пересечения
 * В теории, если отрезки будут реализованы посредством LinkedList, должно работать немного быстрее
 * (т.к. в каждом отрезке мы заменяем либо head, либо tail)
 */
@Component
@Profile("splitedDequeue")
public class SplitedDequeueConveyor extends AbstractConveyor<List<Deque<Integer>>> {

    SplitedDequeueConveyor(
            @Value("#{'${queuea}'.split(',')}") List<Integer> queueA,
            @Value("#{'${queueb}'.split(',')}") List<Integer> queueB,
            @Value("#{'${crosspoints}'.split(',')}") List<Integer> crossPoints) {
        super(queueA, queueB, crossPoints);
    }

    @Override
    public int pushToQueueA(int value) {
        return pushToQueue(queueA, queueB, value);
    }

    @Override
    public int pushToQueueB(int value) {
        return pushToQueue(queueB, queueA, value);
    }

    @Override
    protected List<Deque<Integer>> mapInnerQueue(List<Integer> initial) {
        var subQueues = new ArrayList<Deque<Integer>>(crossPoints.size() + 1);
        int fromIndex = 0;
        for (Integer toIndex : crossPoints) {
            subQueues.add(new ArrayDeque<>(initial.subList(fromIndex, toIndex)));
            fromIndex = toIndex;
        }
        subQueues.add(new ArrayDeque<>(initial.subList(fromIndex, initial.size())));
        return subQueues;
    }

    @SuppressWarnings("ConstantConditions")
    @Override
    protected synchronized int pushToQueue(List<Deque<Integer>> primary, List<Deque<Integer>> secondary, int value) {
        Integer polled = null;
        for (int i = 0; i < primary.size(); i++) {
            var primarySubQueue = primary.get(i);
            var secondarySubQueue = secondary.get(i);
            if (polled != null) {
                primarySubQueue.addFirst(polled);
                secondarySubQueue.removeFirst();
                secondarySubQueue.addFirst(polled);
            } else {
                primarySubQueue.addFirst(value);
            }
            polled = primarySubQueue.pollLast();
        }
        return polled;
    }

    @Override
    protected List<Integer> getQueue(List<Deque<Integer>> queue) {
        return queue.stream().flatMap(Collection::stream).collect(Collectors.toList());
    }
}
