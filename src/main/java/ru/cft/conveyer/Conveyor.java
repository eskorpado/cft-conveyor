package ru.cft.conveyer;

import java.util.List;

public interface Conveyor {

    /**
     * Добавить элемент в очередь A
     */
    int pushToQueueA(int value);

    /**
     * Добавить элемент в очередь B
     */
    int pushToQueueB(int value);

    /**
     * @return Состояние очереди A
     */
    List<Integer> getQueueA();

    /**
     * @return Состояние очереди B
     */
    List<Integer> getQueueB();
}
