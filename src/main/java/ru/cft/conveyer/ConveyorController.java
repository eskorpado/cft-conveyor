package ru.cft.conveyer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ConveyorController {

    private final Conveyor conveyor;
    private final List<Integer> crossPoints;
    public ConveyorController(
            Conveyor conveyor,
            @Value("#{'${crosspoints}'.split(',')}") List<Integer> crossPoints) {
        this.conveyor = conveyor;
        this.crossPoints = crossPoints;
    }

    @PatchMapping("/{selector}")
    public Integer pushToQueue(@PathVariable QueueSelector selector, @RequestParam Integer value) {
        if (value == null) {
            throw new NullPointerException();
        }
        switch (selector) {
            case A:
                return conveyor.pushToQueueA(value);
            case B:
                return conveyor.pushToQueueB(value);
        }
        throw new IllegalArgumentException("Unknown queue");
    }

    @GetMapping("/{selector}")
    public List<Integer> getQueue(@PathVariable QueueSelector selector) {
        switch (selector) {
            case A:
                return conveyor.getQueueA();
            case B:
                return conveyor.getQueueB();
        }
        throw new IllegalArgumentException("Unknown queue");
    }

    @GetMapping("/state")
    public Map<String, List<Integer>> getState() {
        Map<String, List<Integer>> result = new HashMap<>();
        result.put("a", conveyor.getQueueA());
        result.put("b", conveyor.getQueueB());
        result.put("crossPoints", crossPoints);
        return result;
    }
}
