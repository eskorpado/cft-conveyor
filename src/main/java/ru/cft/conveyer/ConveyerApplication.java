package ru.cft.conveyer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConveyerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConveyerApplication.class, args);
    }

}
